from distutils.core import setup

setup(
    name='RoboZap',
    version='0.1',
    packages=[''],
    package_dir={'': 'robozap'},
    url='www.we45.com',
    license='MIT License',
    author='Abhay Bhargav',
    author_email='Twitter: @abhaybhargav',
    description='Robot Framework Library for OWASP ZAP 2.6'
)
